import axios from "axios";
import moment from "moment";
import xml2js from "xml2js";
import xmlbuilder from "xmlbuilder";

export default class Fedex {
  static HOLIDAYS = [
    "2016-03-27",
    "2016-05-30",
    "2016-07-04",
    "2016-09-05",
    "2016-11-24",
    "2016-12-25",
    "2016-12-26",
    "2017-01-01",
    "2017-01-02",
    "2017-07-04",
    "2017-11-23",
    "2017-12-25",
    "2017-12-31",
    "2018-01-01",
  ];

  static CLOSED = [0];

  static NAME = "Fedex";

  public config: any = {};

  public apiURL: string;

  constructor(config: any) {
    this.config = config;
    if (config.type === "testing") {
      this.apiURL = "https://wsbeta.fedex.com/xml";
    } else {
      this.apiURL = "https://ws.fedex.com/xml";
    }
  }

  getRates(
    shipper: any,
    recipient: any,
    orderBins: any[] | null,
    shipDate: any,
    saturday: boolean,
    signature: boolean,
    callback: any,
  ) {
    let num = 1;

    if (!orderBins || orderBins.length === 0) {
      return callback(null, []);
    }

    orderBins.forEach((bin) => {
      bin.num = num++;
    });

    const xmlReq = this.__startRateXML();

    this.__addShipment(
      xmlReq,
      shipper,
      recipient,
      orderBins,
      shipDate,
      saturday,
      signature,
    );

    const req = xmlReq.end();

    let timeout;

    if (saturday) {
      timeout = 10000;
    } else {
      timeout = 12000;
    }

    axios
      .post(this.apiURL, { timeout, responseType: "text", data: req })
      //@ts-ignore
      .then(({ body }) => {
        return xml2js.parseString(body, (err: any, result: any) => {
          callback(err, result);
        });
      })
      .catch((error: { data: string; }) => {
        error.data = "Fedex rates";

        return callback(error);
      });
  }

  __startRateXML() {
    const xml = xmlbuilder
      .create("RateRequest", {
        standalone: true,
        stringify: {
          //@ts-ignore
          eleName: (val: any) => `ns:${val}`,
        },
      })
      .att("xmlns:ns", "http://fedex.com/ws/rate/v16")
      .att("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

    xml.ele({
      WebAuthenticationDetail: {
        UserCredential: {
          Key: this.config.key,
          Password: this.config.password,
        },
      },
      ClientDetail: {
        AccountNumber: this.config.shipaccount,
        MeterNumber: this.config.meter,
      },
      Version: {
        ServiceId: "crs",
        Major: 16,
        Intermediate: 0,
        Minor: 0,
      },
      ReturnTransitAndCommit: true,
    });

    return xml;
  }

  __addShipment(
    xmlReq: { ele: (arg0: { RequestedShipment: { ShipTimestamp: any; DropoffType: any; }; }) => any; },
    shipper: any,
    recipient: any,
    orderBins: any[],
    shipDate: any,
    saturday: boolean,
    signature: boolean,
  ) {
    const rsh = xmlReq.ele({
      RequestedShipment: {
        ShipTimestamp: moment(shipDate).format(),
        DropoffType: this.config.dropoffType,
      },
    });

    if (this.config.hubID) {
      rsh.ele({
        ServiceType: "SMART_POST",
      });
    }

    rsh.ele({
      PackagingType: this.config.packagingType,
      Shipper: Fedex.__toFedexAddress(shipper),
      Recipient: Fedex.__toFedexAddress(recipient),
    });

    if (saturday) {
      rsh.ele({
        SpecialServicesRequested: {
          SpecialServiceTypes: "SATURDAY_DELIVERY",
        },
      });
    }

    if (this.config.hubID) {
      rsh.ele({
        SmartPostDetail: {
          Indicia: "PARCEL_SELECT",
          HubId: this.config.hubID,
        },
      });
    }

    rsh.ele({ PackageCount: orderBins.length });

    orderBins.forEach((orderBin: { weight: number; num: any; width: number; height: number; depth: number; }) => {
      if (orderBin.weight < 28) {
        orderBin.weight = 28;
      }

      let kg = Math.floor(Math.floor(orderBin.weight) / 1000);

      if (Math.floor(orderBin.weight) / 1000 - kg > 0.08) {
        kg = Math.floor(orderBin.weight) / 1000;
      }

      const rli = rsh.ele({
        RequestedPackageLineItems: {
          SequenceNumber: orderBin.num,
          GroupPackageCount: 1,
          Weight: {
            Units: "KG",
            Value: kg,
          },
          Dimensions: {
            Length: Math.round(orderBin.width / 10),
            Width: Math.round(orderBin.height / 10),
            Height: Math.round(orderBin.depth / 10),
            Units: "CM",
          },
          PhysicalPackaging: "BOX",
        },
      });

      if (recipient.country === "US" && signature) {
        rli.ele({
          SpecialServicesRequested: {
            SpecialServiceTypes: "SIGNATURE_OPTION",
            SignatureOptionDetail: {
              OptionType: "DIRECT",
            },
          },
        });
      }
    });
  }

  static __toFedexAddress(src: { name: string | any[]; isResidential: boolean; company: any; phone: any; country: string; street1: any; street2: any; street3: any; city: any; province: any; postalCode: any; }) {
    let personName;

    if (src.name.length > 0) {
      personName = src.name;
    } else {
      personName = "unknown";
    }

    if (!src.isResidential) {
      src.isResidential = true;
    }

    const ad: any = {
      Contact: {
        PersonName: personName,
        CompanyName: src.company,
        PhoneNumber: src.phone,
      },
    };

    if (["US", "CA"].includes(src.country)) {
      ad.Address = {
        StreetLines: [src.street1, src.street2, src.street3].join(", "),
        City: src.city,
        StateOrProvinceCode: src.province,
        PostalCode: src.postalCode,
        CountryCode: src.country,
        Residential: src.isResidential,
      };
    } else {
      ad.Address = {
        StreetLines: [src.street1, src.street2, src.street3].join(", "),
        City: src.city,
        PostalCode: src.postalCode,
        CountryCode: src.country,
        Residential: src.isResidential,
      };
    }

    return ad;
  }
}
