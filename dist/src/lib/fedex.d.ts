import xmlbuilder from "xmlbuilder";
export default class Fedex {
    static HOLIDAYS: string[];
    static CLOSED: number[];
    static NAME: string;
    config: any;
    apiURL: string;
    constructor(config: any);
    getRates(shipper: any, recipient: any, orderBins: any[] | null, shipDate: any, saturday: boolean, signature: boolean, callback: any): any;
    __startRateXML(): xmlbuilder.XMLElement;
    __addShipment(xmlReq: {
        ele: (arg0: {
            RequestedShipment: {
                ShipTimestamp: any;
                DropoffType: any;
            };
        }) => any;
    }, shipper: any, recipient: any, orderBins: any[], shipDate: any, saturday: boolean, signature: boolean): void;
    static __toFedexAddress(src: {
        name: string | any[];
        isResidential: boolean;
        company: any;
        phone: any;
        country: string;
        street1: any;
        street2: any;
        street3: any;
        city: any;
        province: any;
        postalCode: any;
    }): any;
}
