"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const xml2js_1 = __importDefault(require("xml2js"));
const xmlbuilder_1 = __importDefault(require("xmlbuilder"));
class Fedex {
    constructor(config) {
        this.config = {};
        this.config = config;
        if (config.type === "testing") {
            this.apiURL = "https://wsbeta.fedex.com/xml";
        }
        else {
            this.apiURL = "https://ws.fedex.com/xml";
        }
    }
    getRates(shipper, recipient, orderBins, shipDate, saturday, signature, callback) {
        let num = 1;
        if (!orderBins || orderBins.length === 0) {
            return callback(null, []);
        }
        orderBins.forEach((bin) => {
            bin.num = num++;
        });
        const xmlReq = this.__startRateXML();
        this.__addShipment(xmlReq, shipper, recipient, orderBins, shipDate, saturday, signature);
        const req = xmlReq.end();
        let timeout;
        if (saturday) {
            timeout = 10000;
        }
        else {
            timeout = 12000;
        }
        axios_1.default
            .post(this.apiURL, { timeout, responseType: "text", data: req })
            //@ts-ignore
            .then(({ body }) => {
            return xml2js_1.default.parseString(body, (err, result) => {
                callback(err, result);
            });
        })
            .catch((error) => {
            error.data = "Fedex rates";
            return callback(error);
        });
    }
    __startRateXML() {
        const xml = xmlbuilder_1.default
            .create("RateRequest", {
            standalone: true,
            stringify: {
                //@ts-ignore
                eleName: (val) => `ns:${val}`,
            },
        })
            .att("xmlns:ns", "http://fedex.com/ws/rate/v16")
            .att("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xml.ele({
            WebAuthenticationDetail: {
                UserCredential: {
                    Key: this.config.key,
                    Password: this.config.password,
                },
            },
            ClientDetail: {
                AccountNumber: this.config.shipaccount,
                MeterNumber: this.config.meter,
            },
            Version: {
                ServiceId: "crs",
                Major: 16,
                Intermediate: 0,
                Minor: 0,
            },
            ReturnTransitAndCommit: true,
        });
        return xml;
    }
    __addShipment(xmlReq, shipper, recipient, orderBins, shipDate, saturday, signature) {
        const rsh = xmlReq.ele({
            RequestedShipment: {
                ShipTimestamp: (0, moment_1.default)(shipDate).format(),
                DropoffType: this.config.dropoffType,
            },
        });
        if (this.config.hubID) {
            rsh.ele({
                ServiceType: "SMART_POST",
            });
        }
        rsh.ele({
            PackagingType: this.config.packagingType,
            Shipper: Fedex.__toFedexAddress(shipper),
            Recipient: Fedex.__toFedexAddress(recipient),
        });
        if (saturday) {
            rsh.ele({
                SpecialServicesRequested: {
                    SpecialServiceTypes: "SATURDAY_DELIVERY",
                },
            });
        }
        if (this.config.hubID) {
            rsh.ele({
                SmartPostDetail: {
                    Indicia: "PARCEL_SELECT",
                    HubId: this.config.hubID,
                },
            });
        }
        rsh.ele({ PackageCount: orderBins.length });
        orderBins.forEach((orderBin) => {
            if (orderBin.weight < 28) {
                orderBin.weight = 28;
            }
            let kg = Math.floor(Math.floor(orderBin.weight) / 1000);
            if (Math.floor(orderBin.weight) / 1000 - kg > 0.08) {
                kg = Math.floor(orderBin.weight) / 1000;
            }
            const rli = rsh.ele({
                RequestedPackageLineItems: {
                    SequenceNumber: orderBin.num,
                    GroupPackageCount: 1,
                    Weight: {
                        Units: "KG",
                        Value: kg,
                    },
                    Dimensions: {
                        Length: Math.round(orderBin.width / 10),
                        Width: Math.round(orderBin.height / 10),
                        Height: Math.round(orderBin.depth / 10),
                        Units: "CM",
                    },
                    PhysicalPackaging: "BOX",
                },
            });
            if (recipient.country === "US" && signature) {
                rli.ele({
                    SpecialServicesRequested: {
                        SpecialServiceTypes: "SIGNATURE_OPTION",
                        SignatureOptionDetail: {
                            OptionType: "DIRECT",
                        },
                    },
                });
            }
        });
    }
    static __toFedexAddress(src) {
        let personName;
        if (src.name.length > 0) {
            personName = src.name;
        }
        else {
            personName = "unknown";
        }
        if (!src.isResidential) {
            src.isResidential = true;
        }
        const ad = {
            Contact: {
                PersonName: personName,
                CompanyName: src.company,
                PhoneNumber: src.phone,
            },
        };
        if (["US", "CA"].includes(src.country)) {
            ad.Address = {
                StreetLines: [src.street1, src.street2, src.street3].join(", "),
                City: src.city,
                StateOrProvinceCode: src.province,
                PostalCode: src.postalCode,
                CountryCode: src.country,
                Residential: src.isResidential,
            };
        }
        else {
            ad.Address = {
                StreetLines: [src.street1, src.street2, src.street3].join(", "),
                City: src.city,
                PostalCode: src.postalCode,
                CountryCode: src.country,
                Residential: src.isResidential,
            };
        }
        return ad;
    }
}
exports.default = Fedex;
Fedex.HOLIDAYS = [
    "2016-03-27",
    "2016-05-30",
    "2016-07-04",
    "2016-09-05",
    "2016-11-24",
    "2016-12-25",
    "2016-12-26",
    "2017-01-01",
    "2017-01-02",
    "2017-07-04",
    "2017-11-23",
    "2017-12-25",
    "2017-12-31",
    "2018-01-01",
];
Fedex.CLOSED = [0];
Fedex.NAME = "Fedex";
//# sourceMappingURL=fedex.js.map